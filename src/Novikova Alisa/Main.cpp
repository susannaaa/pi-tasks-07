#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "Employee.h"
#include "Manager.h"
#include "Team.h"
#include "Stuff.h"
#define N 32
#define NUM 44
#define lenghtFirst 69
using namespace std;

int main()
{
	srand(time(NULL));
	Employee* employees[NUM];
	int mgDOW = 0; // ���������� ���������� �� ������� DOW
	int mgUP = 0; // ���������� ���������� �� ������� UP
	int engDOW = 0; // ���������� ������������� � �������� �� ������� DOW
	int engUP = 0; // ���������� ������������� � �������� �� ������� UP
	ifstream file("AllStuff.txt");
	if (file.is_open() == false)
	{
		cout << "File cannot be opened" << endl;
		exit(EXIT_FAILURE);
	}
	// �������� ��������� � ����� ������������ ��� ������ 
	file.seekg(lenghtFirst, ios::beg); 
	// ������� ���������� ���������� � ��������� �� ������ �������
	for (int i = 0; file.eof() == false; i++)
	{
		string fio, surname, name, lastname, position, project;
		int id, base;
		file >> id >> surname >> name >> lastname >> position >> project >> base;
		if (position == "Manager")
		{
			if (project == "DOW")
				mgDOW++;
			if (project == "UP")
				mgUP++;
		}
		if (position == "Programmer" || position == "Tester")
		{
			if (project == "DOW")
				engDOW++;
			if (project == "UP")
				engUP++;
		}
	}
	file.seekg(lenghtFirst, ios::beg); // ������� � ��������� ��������� � �����
	// ������ �� ����� � ���������� �����������
	for (int i = 0; file.eof() == false; i++)
	{
		string fio, surname, name, lastname, position, project;
		int id, base;
		int numberMN; // ���������� ����������
		int numberEng; // ���������� ���������
		long int budget = 0;
		file >> id >> surname >> name >> lastname >> position >> project >> base;
		fio = surname + " " + name + " " + lastname;

		//���������� ������� � ��������� � ����������� �� �������
		if (project == "DOW")
		{
			budget = 1000000;
			numberMN = mgDOW;
			numberEng = engDOW;
		}
		if (project == "UP")
		{
			budget = 3000000;
			numberMN = mgUP;
			numberEng = engUP;
		}
		//���� �� ��������� � �������� 
		if (project == "NOP") 
			budget = 4000000;

		// ��������� �������� � ���������� ��������
		if (position == "Cleaner")
		{
			employees[i] = new Cleaner(id, fio, base);
			employees[i]->SetPosition("Cleaner");
			continue;
		}
		if (position == "Driver")
		{
			employees[i] = new Driver(id, fio, base);
			employees[i]->SetPosition("Driver");
			continue;
		}
		if (position == "Programmer")
		{
			employees[i] = new Programmer(id, fio, project, base);
			employees[i]->SetPosition("Programmer");
			continue;
		}
		if (position == "Tester")
		{
			employees[i] = new Tester(id, fio, project, base);
			employees[i]->SetPosition("Tester");
			continue;
		}
		if (position == "Team_Leader")
		{
			employees[i] = new TeamLeader(id, fio, project, base, budget, numberEng);
			employees[i]->SetPosition("Team Leader");
			continue;
		}
		if (position == "Manager")
		{
			employees[i] = new Manager(id, fio, project, budget);
			employees[i]->SetPosition("Manager");
			continue;
		}
		if (position == "Senior_Manager")
		{
			employees[i] = new SeniorManager(id, fio, project, budget);
			employees[i]->SetPosition("Senior Manager");
			continue;
		}
		if (position == "Project_Manager")
		{
			employees[i] = new ProjectManager(id, fio, project, budget, numberMN);
			employees[i]->SetPosition("Project Manager");
			continue;
		}
	}


	// ����� ���������� �� �������
	ofstream newFile("Staff.txt");
	cout << "N" << "    " << "Name";
	newFile << "N" << "    " << "Name";
	for (int i = 0; i < N; i++)
	{
		cout << " ";
		newFile << " ";
	}
	cout << "  Position\n"<<endl;
	newFile << "  Position\n" << endl;
	
	
	for (int i = 0; i < NUM; i++){
		int lengnt = employees[i]->GetName().length();

		if (employees[i]->getID() < 10)
		{
			cout << employees[i]->getID() << "    " << employees[i]->GetName();
			newFile << employees[i]->getID() << "    " << employees[i]->GetName();
		}
		else
		{
			cout << employees[i]->getID() << "   " << employees[i]->GetName();
			newFile << employees[i]->getID() << "   " << employees[i]->GetName();
		}
		for (int j = lengnt; j < 35; j++)
		{
			cout << " ";
			newFile << " ";
		}
		cout << employees[i]->GetPosition()<<" : ";
		newFile << employees[i]->GetPosition()<<" : ";
		
		employees[i]->Pay();
		cout << "salary-" << employees[i]->GetPayment();
		newFile << " salary-" << employees[i]->GetPayment();
		
		cout <<" timework=" << employees[i]->GetTime() << "h    " << endl;
		newFile <<" timework=" << employees[i]->GetTime() << "h    " << endl;
		
		cout << endl;
		newFile << endl;
	}
	newFile.close();

	return 0;
}