#pragma once

#include "Employee.h"

class Manager : public Employee, public Project
{
	protected:
		double part;
		string proj;
	public:
		Manager(int id = 0, string name = "", string proj = "", double part = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->proj = proj;
			this->worktime = worktime;
			this->payment = payment;
			this->part = part;
		}
		~Manager() {}
		void setPart(double part) { this->part = part; }
		void setProj(string proj) { this->proj = proj; }
		double getPart() const { return part; }
		string getProj() const { return proj; }
		double salary_type_2(int budget, double part) override
		{
			return budget*part / per;
		}
		void calculate() override
		{
			this->payment = salary_type_2(amount, part)*ratio2;
		}
};

class ProjectManager : public Manager, public Heading
{
	protected:
		int qual;
	public:
		ProjectManager(int id = 0, string name = "", string proj = "", int qual = 0, double part = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->proj = proj;
			this->qual = qual;
			this->worktime = worktime;
			this->payment = payment;
			this->part = part;
		}
		~ProjectManager() {}
		double salary_type_3(int qual) override
		{
			return qual*amount;
		}
		void calculate() override
		{
			this->payment = (salary_type_2(amount, part) + salary_type_3(qual))*ratio1;
		}
};

class SeniorManager : public ProjectManager
{
	public:
		SeniorManager(int id = 0, string name = "", string proj = "", int qual = 0, double part = 0, int worktime = 0, int payment = 0)
		{
			this->id = id;
			this->name = name;
			this->proj = proj;
			this->qual = qual;
			this->worktime = worktime;
			this->payment = payment;
			this->part = part;
		}
		~SeniorManager() {}
};

