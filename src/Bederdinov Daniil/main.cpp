#define NUMBOFSTAFF 41
#include <fstream>
#include "Employee.h"
#include "Cleaner.h"
#include "Driver.h"
#include "TeamLeader.h"
#include "Tester.h"
#include "Programmer.h"
#include "Personal.h"
#include "Manager.h"
#include "Engineer.h"
#include "ProjectManager.h"
#include "SeniorManager.h"
using namespace std;


int ToInt32(string& str)
{
    return atoi(str.c_str());
}

double ToDouble(string& str)
{
    return atof(str.c_str());
}

int main()
{
    Employee *staff[NUMBOFSTAFF];
    int i = 0;
    ifstream Data("C:\\!!!Projects\\StaffDemo\\Debug\\data.txt");
    if (!Data.is_open())
    {
        cout << "Error" << endl;
        exit(EXIT_FAILURE);
    }
    string Id, Name, Proffesion, WorkTime, Payment, space;
    while (!Data.eof())
    {
        getline(Data, Id);
        getline(Data, Name);
        getline(Data, Proffesion);
        getline(Data, WorkTime);
        getline(Data, Payment);
        if (Proffesion == "Driver")
        {
            string Base;
            getline(Data, Base);
            staff[i++] = new Driver(Id, Name, ToInt32(WorkTime), ToDouble(Payment), ToInt32(Base));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "Cleaner")
        {
            string Base;
            getline(Data, Base);
            staff[i++] = new Cleaner(Id, Name, ToInt32(WorkTime), ToDouble(Payment), ToInt32(Base));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "Programmer")
        {
            string Base, projTitle, part, projBudg;
            getline(Data, projTitle);
            getline(Data, part);
            getline(Data, Base);
            getline(Data, projBudg);
            staff[i++] = new Programmer(Id, Name, ToInt32(WorkTime), ToDouble(Payment), projTitle,ToDouble(part), ToInt32(Base), ToInt32(projBudg));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "Tester")
        {
            string Base, projTitle, part, projBudg;
            getline(Data, projTitle);
            getline(Data, part);
            getline(Data, Base);
            getline(Data, projBudg);
            staff[i++] = new Tester(Id, Name, ToInt32(WorkTime), ToDouble(Payment), projTitle,ToDouble(part), ToInt32(Base), ToInt32(projBudg));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "TeamLeader")
        {
            string Base, projTitle, part, projBudg, subNumber, feeForOne;
            getline(Data, projTitle);
            getline(Data, part);
            getline(Data, Base);
            getline(Data, projBudg);
            getline(Data, subNumber);
            getline(Data, feeForOne);
            staff[i++] = new TeamLeader(Id, Name, ToInt32(WorkTime), ToDouble(Payment), projTitle,ToDouble(part), ToInt32(Base),
                ToInt32(projBudg), ToInt32(subNumber), ToInt32(feeForOne));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "Manager")
        {
            string curBase, projTitle, part, projBudg;
            getline(Data, projTitle);
            getline(Data, part);
            getline(Data, projBudg);
            staff[i++] = new Manager(Id, Name, ToInt32(WorkTime), ToDouble(Payment), projTitle, ToDouble(part), ToInt32(projBudg));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "ProjectManager")
        {
            string projTitle, part, projBudg, subNumber, feeForOne;
            getline(Data, projTitle);
            getline(Data, part);
            getline(Data, projBudg);
            getline(Data, subNumber);
            getline(Data, feeForOne);
            staff[i++] = new ProjectManager(Id, Name, ToInt32(WorkTime), ToDouble(Payment), projTitle, ToDouble(part), ToInt32(projBudg),
                ToInt32(subNumber), ToInt32(feeForOne));
            getline(Data, space);
            continue;
        }
        if (Proffesion == "SeniorManager")
        {
            string projTitle, part, projBudg, subNumber, feeForOne, projNum;
            getline(Data, projTitle);
            getline(Data, part);
            getline(Data, projBudg);
            getline(Data, subNumber);
            getline(Data, feeForOne);
            getline(Data, projNum);
            staff[i++] = new SeniorManager(Id, Name, ToInt32(WorkTime), ToDouble(Payment), projTitle, ToDouble(part), ToInt32(projBudg),
                ToInt32(subNumber), ToInt32(feeForOne), ToInt32(projNum));
            getline(Data, space);
            continue;
        }
    }
    Data.close();


    for (int i = 0; i < NUMBOFSTAFF; i++)
    {
        staff[i]->CalcPayment();
        staff[i]->Print_Info();
    }
    return 0;
}

