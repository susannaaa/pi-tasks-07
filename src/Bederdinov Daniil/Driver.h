#ifndef DRV
#define DRV
#include "Personal.h"
#include <string>
using namespace std;

class Driver:public Personal
{

public:
    Driver() : Personal() {};
    Driver(string id, string name, int workTime, double payment, int base) : Personal(id, name, workTime, payment, base){};

    void Print_Info() override
    {
        cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Driver" << endl
        << "Work Time: " << GetWorkTime() << " hours" << endl << "Base: " << base << endl << "Salary : " << GetPayment() << endl;
    }

};
#endif
