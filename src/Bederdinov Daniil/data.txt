1
Grishaeva A. N.
Cleaner
30
0
200

2
Kozlov I. U.
Driver
33
0
170

3
Vasilyev O. A.
Programmer
44
0
Project #1
0.1
300
500000

4
Krisin V. V.
Programmer
40
0
Project #1
0.1
310
500000

5
Malinov K. N.
Tester
20
0
Project #1
0.1
310
500000

6
Petryuk A. P.
TeamLeader
50
0
Project #1
0.15
330
500000
3
200

7
Vidrin V. L.
Manager
12
0
Project #1
0.2
500000

8
Kostilev I. R.
ProjectManager
29
0
Project #1
0.25
500000
5
300

9
Pojarkin F. J.
Programmer
37
0
Project #2	
0.1
315
500000

10
Shertnev E. T.
Programmer
33
0
Project #2
0.1
320
500000

11
Murzilkin H. Q.
Tester
25
0
Project #2
0.1
315
500000

12
Lososev B. D.
TeamLeader
36
0
Project #2
0.15
330
500000
3
220

13
Besov X. U.
Manager
14
0
Project #2
0.2
500000

14
Pochkin I. E.
ProjectManager
32
0
Project #2
0.25
500000
5
330

15
Fedorov G. A.
SeniorManager
46
0
Project #1, Project #2
0.1
500000
12
300
2

16
Kazulkin R. T.
Programmer
44
0
Project #3
0.1
320
700000

17
Suchkov H. L.
Programmer
35
0
Project #3
0.1
315
700000

18
Varvarov L. M.
Tester
32
0
Project #3
0.1
325
700000

19
Rechnov Y. E.
TeamLeader
36
0
Project #3
0.15
330
700000
3
280

20
Bezrotov P. O.
Manager
11
0
Project #3
0.2
700000

21
Sumkin V. C.
ProjectManager
35
0
Project #3
0.25
700000
5
350

22
Rachkov B. S.
Programmer
47
0
Project #4
0.1
340
700000

23
Sotkin V. G.
Programmer
33
0
Project #4
0.1
325
700000

24
Antarktidin E. N.
Tester
23
0
Project #4
0.1
330
700000

25
Daunich A. O.
TeamLeader
32
0
Project #4
0.15
345
700000
3
280

26
Pevcov R. I. 
Manager
7
0
Project #4
0.2
700000

27
Ris V. L.
ProjectManager
31
0
Project #4
0.25
700000
5
400

28
Faker Y. K.
SeniorManager
41
0
Project #3, Project #4
0.1
700000
12
350
2

29
Irodov S. S.
Driver
30
0
250

30
Satirov F. F.
Driver
26
0
265

31
Dakteypov M. N.
Programmer
40
0
Project #5
0.2
330
750000

32
Tarasov G. G.
Tester
28
0
Project #5
0.2
330
750000

33
Gastritov A. A.
TeamLeader
45
0
Project #5
0.2
350
750000
2
290

34
Nekatinov L. C.
Manager
20
0
Project #5
0.1
750000

35
Troyanov H. J.
ProjectManager
34
0
Project #5
0.2
750000
4
400

36
Teroristov E. B.
Programmer
42
0
Project #6
0.11
700
750000

37
Lichinkov O. T.
Tester
23
0
Project #6
0.09
650
750000

38
Valerchenko Y. W.
TeamLeader
34
0
Project #6
0.2
750
750000
2
250

39
Napravo M. D.
Manager
27
0
Project #6
0.2
750000

40
Lepyohin C. C.
ProjectManager
43
0
Project #6
0.3
750000
4
400

41
Kartoshka V. L.
SeniorManager
55
0
Project #5, Project #6
0.1
750000
5
700
2