#ifndef CLR
#define CLR
#include "Personal.h"
#include <string>
using namespace std;

class Cleaner:public Personal
{

public:
    Cleaner() : Personal() {};
    Cleaner(string id, string name, int workTime, double payment, int base) : Personal(id, name, workTime, payment, base) {};

    void Print_Info() override
    {
        cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Cleaner" << endl
        << "Work Time: " << GetWorkTime() << " hours" << endl<< "Base: " << base << endl<<"Salary : " << GetPayment() << endl;
    }
};
#endif
