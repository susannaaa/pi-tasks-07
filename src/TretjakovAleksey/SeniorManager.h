#pragma once
#include "Employee.h"
#include "Heading.h"
#include "ProjectManager.h"
#include <iostream>
#include <string>
using namespace std;

class SeniorManager : public ProjectManager
{
private:
	int projCount;
public:
	SeniorManager(string id, string name, int workTime, double payment, string projectName, double part, int budget, int number, int stakeForOne, int projCount)
		:ProjectManager(id, name, workTime, payment, projectName, part, budget, number, stakeForOne) // ����������� �������������� � ���������� ���������
	{
		this->projCount = projCount;
	}


	double ProjectSalary(int budget, double part) override // ������ ���.����� �� ������� �������, �������� ������� � ���������� ��������
	{
		return budget * part * projCount;
	}

	void Print_Info() override // ����� ���������� � �������
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Senior Manager" << endl
		<< "Projects Titles: " << projectName << endl << "Projects Budget: " << budget << endl << "Participation in projects: " << part << endl
		<< "Projects Number: " << projCount << endl << "Employees Number: " << number << endl
		<< "Stake for one: " << stakeForOne << endl << "Work Time: " << GetWorkTime() << " hours" << endl << "Salary: " << GetPayment() << endl;
	}
};