#pragma once
#include "Employee.h"
#include "Project.h"
#include <iostream>
#include <string>
using namespace std;

class Manager : public Employee, public Project
{
protected:
	string projectName;
	double part;
	int budget;
public:
	Manager(string id, string name, int workTime, double payment, string projectName, double part, int budget) :Employee(id, name, workTime, payment)
		// ����������� �������������� �� ������� + �������� �������, ������� ������� � ������ �������
	{
		this->projectName = projectName;
		this->part = part;
		this->budget = budget;
	}

	double ProjectSalary(int budget, double part) override // ������ ���.����� �� ������� ������� � �������� �������
	{
		return budget * part;
	}

	void CalcPayment() override  // ������ � ��������� ����� ���.�����
	{
		SetPayment(ProjectSalary(budget, part));
	}

	void Print_Info() override  // ����� ���������� � �������
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession:  Manager" << endl
		<< "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
		<< "Work Time: " << GetWorkTime() << " hours" << endl<< "Salary: " << GetPayment() << endl;
	}

};