#pragma once
#include <iostream>
using namespace std;
class Employee
{
protected:
	string id;
	string name;
	int workTime;
	double payment;

public:
	Employee(string id, string name, int workTime, double payment) // ����������� �� ����, �����, ������� ������ � ���.�����
	{
		this->id = id;
		this->name = name;
		this->workTime = workTime;
		this->payment = payment;
	}

	Employee(int id, string name) // ����������� �� ���� � �����
	{
		this->id = id;
		this->name = name;
		workTime = 0;
		payment = 0;
	}
	void SetWorkTime(int time) // ������ ������� ������
	{
		workTime = time;
	}

	int GetWorkTime() const // ������ ������� ������
	{
		return workTime;
	}

	void SetId(int _id) // ������ ����
	{
		id = _id;
	}

	string GetId() const // ������ ����
	{
		return id;
	}

	void SetPayment(double _payment) // ������ ���.�����
	{
		payment = _payment;
	}

	double GetPayment() const // ������ ���.�����
	{
		return payment;
	}

	void SetName(string _name) // ������ �����
	{
		name = _name;
	}

	string GetName() const // ������ �����
	{
		return name;
	}

	virtual void CalcPayment() = 0; // ����������� ����� ������� ���.�����
	virtual void Print_Info() = 0;  // ����������� ����� ������ ����������
};