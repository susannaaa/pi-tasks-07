#pragma once
#include "Employee.h"
#include "Project.h"
#include "WorkTIme.h"
using namespace std;
class Engineer : public Employee, public WorkTime, public Project
{
protected:
	string projectName;
	double part;
	int base;
	int budget;
public:
	Engineer(string id, string name, int workTime, double payment, string projectName, double part, int base, int budget) :Employee(id, name, workTime, payment)
		// ����������� �������������� �� ������� + �������� �������, ������� ������� � ������ �������
	{
		this->projectName = projectName;
		this->part = part;
		this->base = base;
		this->budget = budget;
	}

	int WorkTimeSalary(int base, int workTime) override   // ������ ���.����� �� ������ � ������� ������
	{
		return workTime * base;
	}
	double ProjectSalary(int budget, double part) override  // ������ ���.����� �� ������� ������� � �������� �������
	{
		return budget * part;
	}

	void CalcPayment() override  // ������ � ��������� ����� ���.�����
	{
		SetPayment(WorkTimeSalary(GetWorkTime(), base) + ProjectSalary(budget, part));
	}

};