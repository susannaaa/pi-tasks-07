#pragma once
#include "Employee.h"
#include "WorkTime.h"
using namespace std;
class Personal : public Employee, public WorkTime
{
protected:
	int base;

public:
	Personal(string id, string name, int workTime, double payment, int base) : Employee(id, name, workTime, payment), base(base) {}; // ����������� �������������� � �������


	void SetBase(int base) // ������ ������� ������
	{
		this->base = base;
	}

	int GetBase() // ������ ������� ������
	{
		return base;
	}

	int WorkTimeSalary(int base, int workTime) // ������ ���.����� �� ������ � ������� ������
	{
		return base*workTime;
	}

	void CalcPayment() override // ������ � ��������� ����� ���.�����
	{
		SetPayment(WorkTimeSalary(GetWorkTime(), base));
	}

};