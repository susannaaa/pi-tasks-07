#pragma once
#include "Engineer.h"
#include <string>
using namespace std;
class Tester : public Engineer
{
public:
	Tester(string id, string name, int workTime, double payment, string projectName, double part, int base, int budget)
		:Engineer(id, name, workTime, payment, projectName, part, base, budget) {}; // ����������� �������������� �� ��������

	void Print_Info() override // ����� ���������� � �������
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Tester" << endl
			<< "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
			<< "Work Time: " << GetWorkTime() << " hours" << endl << "Base: " << base << endl << "Salary : " << GetPayment() << endl;
	}
};