#pragma once
#include "Programmer.h"
#include "Heading.h"
#include <iostream>
#include <string>
using namespace std;
class TeamLeader : public Programmer, public Heading
{
private:
	int number;
	int stakeForOne;
public:
	TeamLeader(string id, string name, int workTime, double payment, string projectName, double part, int base, int budget, int number, int stakeForOne)
		:Programmer(id, name, workTime, payment, projectName, part, base, budget) // ����������� �������������� �� �����������
	{
		this->number = number;
		this->stakeForOne = stakeForOne;
	}

	int EmplNumbSalary(int stakeForOne, int number) override // ������� ���.����� ������ �� ���������� ����������� � ������ �� ������ ������������
	{
		return stakeForOne * number;
	}

	void CalcPayment() override // ������ � ��������� ����� ���.�����
	{
		SetPayment(WorkTimeSalary(GetWorkTime(), base) + ProjectSalary(budget, part) + EmplNumbSalary(number, stakeForOne));
	}

	void Print_Info() override // ����� ���������� � �������
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Team Leader" << endl
			<< "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
			<< "Employees Number: " << number << endl << "Stake for one: " << stakeForOne << endl
			<< "Work Time: " << GetWorkTime() << " hours" << endl << "Base: " << base << endl << "Salary: " << GetPayment() << endl;
	}
};