#pragma once
#include "Employee.h"
#include "Heading.h"
#include "Manager.h"
#include <iostream>
#include <string>
using namespace std;

class ProjectManager : public Manager, public Heading
{
protected:
	int number;
	int stakeForOne;
public:
	ProjectManager(string id, string name, int workTime, double payment, string projectName, double part, int budget,
		int number, int stakeForOne) : Manager(id, name, workTime, payment, projectName, part, budget)  // ����������� �������������� �� ���������
	{
		this->number = number;
		this->stakeForOne = stakeForOne;
	}

	int EmplNumbSalary(int stakeForOne, int number) override // ������� ���.����� ������ �� ���������� ����������� � ������ �� ������ ������������
	{
		return stakeForOne * number;
	}

	void CalcPayment() override // ������ � ��������� ����� ���.�����
	{
		SetPayment(EmplNumbSalary(stakeForOne, number) + ProjectSalary(budget, part));
	}

	void Print_Info() override // ����� ���������� � �������
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Project Manager" << endl
		<< "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
		<< "Employees Number: " << number << endl << "Stake for one: " << stakeForOne << endl
		<< "Work Time: " << GetWorkTime() << " hours" << endl << "Salary: " << GetPayment() << endl;
	}
};