#pragma once
#include "Engineer.h"
#include <string>
using namespace std;
class Programmer : public Engineer
{
public:
	Programmer(string id, string name, int workTime, double payment, string projectName, double part, int base, int budget)
		:Engineer(id, name, workTime, payment, projectName, part, base, budget) {}; // ����������� �������������� �� ��������

	void Print_Info() override // ����� ���������� � �������
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Programmer" << endl
		<< "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
		<< "Work Time: " << GetWorkTime() << " hours" << endl << "Base: " << base << endl << "Salary : " << GetPayment() << endl;
	}
};